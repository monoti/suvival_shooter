﻿using UnityEngine;
using System.Collections;

public class BossSkillController : MonoBehaviour {

	public int amount = 5;
	float timeSec;
	// Use this for initialization
	void Start () {
		timeSec = 0;
	}
	
	// Update is called once per frame
	void Update () {
		//time += Time.deltaTime;
	}


	void OnTriggerStay(Collider other)
	{
//		Debug.Log("iiiin in");
		if(other.gameObject.tag == "Player")
		{
//			Debug.Log("in in");
			timeSec += Time.deltaTime;
			if(timeSec >= 1)
			{
				Debug.Log(" 1 sec");
				PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
				playerHealth.TakeDamage(5);
				timeSec = 0;
			}
		}

	}
		
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			Debug.Log("out");
			timeSec = 0;
		}
	}


}
