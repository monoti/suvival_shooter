﻿using UnityEngine;
using System.Collections;

public class EnemyAttackRange : MonoBehaviour {
	public float timeBetweenAttacks = 1.0f;
	public int attackDamage = 10;
	public float Distance;
	public GameObject bullet;
	public Transform pointbullet;
	public float speed = 35;
	
	
	Animator anim;
	GameObject player;
	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;
	bool playerInRange;
	float timer;
	float dist;
	NavMeshAgent nav;
	float tempSpeed;
	
	
	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		enemyHealth = GetComponent<EnemyHealth>();
		anim = GetComponent <Animator> ();
		nav = GetComponent <NavMeshAgent> ();
		tempSpeed = nav.speed;
	}
	
	
	void OnTriggerEnter (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = true;
		}
	}
	
	
	void OnTriggerExit (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = false;
		}
	}
	
	
	void Update ()
	{
		timer += Time.deltaTime;
		
		dist = Vector3.Distance (player.transform.position, this.transform.position);
		if(timer >= timeBetweenAttacks && dist <= Distance+2 && enemyHealth.currentHealth > 0)
		{
			//Attack ();
			nav.speed = 0;
			this.transform.LookAt(player.transform.position);
			anim.speed = 2;
			ShootAttack();
		}else if(dist>Distance+2 && enemyHealth.currentHealth > 0){
			anim.speed = 1;
			nav.speed = tempSpeed;
			anim.SetTrigger ("Move");
		}
		
		if(playerHealth.currentHealth <= 0)
		{
			anim.SetTrigger ("PlayerDead");
		}
	}
	
	void ShootAttack(){
		timer = 0f;
		if(playerHealth.currentHealth > 0)
		{
			anim.SetTrigger ("Attack");
			GameObject fire = (GameObject) GameObject.Instantiate( bullet ,pointbullet.position, Quaternion.identity);
			fire.rigidbody.AddForce ((player.transform.position-this.transform.position)*speed);
		}
	}
	
	void Attack ()
	{
		timer = 0f;
		
		if(playerHealth.currentHealth > 0)
		{
			playerHealth.TakeDamage (attackDamage);
		}
	}
}