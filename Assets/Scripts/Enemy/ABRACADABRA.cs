﻿using UnityEngine;
using System.Collections;

public class ABRACADABRA : MonoBehaviour {
	public GameObject xArea;
	public float detectedArea;
	GameObject xA;
	float time;
	GameObject player;
	float ranX;
	float ranZ;
	Vector3 offset;
	public int skillTime;
	// Use this for initialization
	void Start () {
		time = 0;
		skillTime = 2;
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		 
		time += Time.deltaTime;
		if (time > skillTime) {

			ranX = Random.Range(0,detectedArea);
			ranZ = Random.Range(0,detectedArea);
			offset = new Vector3(ranX,0.4f,ranZ);
			xA = (GameObject)GameObject.Instantiate(xArea,player.transform.position+offset,Quaternion.identity);
			time = 0;
			//temps[i].rigidbody.AddForce((directions[i].transform.position-gameObject.transform.position).normalized*530);
		}
	}
}
