﻿using UnityEngine;
using System.Collections;

public class RangeAttack : MonoBehaviour {
	
	public float disappear = 1.75f;
	public int amount = 10;
	// Use this for initialization
	void Start () {
		Destroy (gameObject, disappear);
	}
	
	// Update is called once per frame
	void Update () {
		
		
	}
	
	void OnCollisionEnter(Collision other)
	{
//		Debug.Log ("in");
		if(other.gameObject.tag == "Player")
		{
			Debug.Log("in player");
			PlayerHealth playerHealth = other.gameObject.GetComponent<PlayerHealth>();
			playerHealth.TakeDamage(amount);
			Destroy(gameObject);
		}
	}
}
