﻿using UnityEngine;
using System.Collections;

public class BossAttack : MonoBehaviour {
	public float timeBetweenAttacks = 0.5f;
	public int attackDamage = 10;
	
	
	Animator anim;
	GameObject player;
	PlayerHealth playerHealth;
	EnemyHealth enemyHealth;
	bool playerInRange;
	float timer;
	string moveOrRun;
	NavMeshAgent nav;
	bool update = false;

	public float speedUpdate = 5;
	
	
	void Awake ()
	{
		player = GameObject.FindGameObjectWithTag ("Player");
		playerHealth = player.GetComponent <PlayerHealth> ();
		enemyHealth = GetComponent<EnemyHealth>();
		anim = GetComponent <Animator> ();
		nav = GetComponent <NavMeshAgent> ();
		moveOrRun = "Move";
	}
	
	
	void OnTriggerEnter (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = true;
			anim.SetTrigger ("Attack");
		}
	}
	
	
	void OnTriggerExit (Collider other)
	{
		if(other.gameObject == player)
		{
			playerInRange = false;
			anim.SetTrigger (moveOrRun);
		}
	}
	
	
	void Update ()
	{
		timer += Time.deltaTime;
		
		if(timer >= timeBetweenAttacks && playerInRange && enemyHealth.currentHealth > 0)
		{
			Attack ();
		}

		if(enemyHealth.currentHealth <= enemyHealth.startingHealth/2 && !update && enemyHealth.currentHealth > 0){
			moveOrRun = "Run";
			nav.speed = speedUpdate;
			attackDamage = attackDamage*2;
			anim.SetTrigger (moveOrRun);
			update = true;
		}
		
		if(playerHealth.currentHealth <= 0)
		{
			anim.SetTrigger ("PlayerDead");
		}
	}
	
	
	void Attack ()
	{
		timer = 0f;
		
		if(playerHealth.currentHealth > 0)
		{
			playerHealth.TakeDamage (attackDamage);
		}
	}
}
