﻿using UnityEngine;
using System.Collections.Generic;

public class EnemyHealth : MonoBehaviour
{
	public int startingHealth = 100;
	public int currentHealth;
	public float sinkSpeed = 2.5f; //sink to floor when dead
	public int scoreValue = 10;
	public AudioClip deathClip;
	public List<EnemyState.State> killStateSet;
	
	Animator anim;
	AudioSource enemyAudio;
	ParticleSystem hitParticles;
	CapsuleCollider capsuleCollider;
	bool isDead;
	bool isSinking;
	public Dictionary<EnemyState.State, EnemyState> stateSet;
	
	public ParticleSystem burnEffect = null;
	public ParticleSystem slowEffect = null;
	public ParticleSystem stunEffect = null;
	
	
	void Awake ()
	{
		anim = GetComponent <Animator> ();
		enemyAudio = GetComponent <AudioSource> ();
		hitParticles = GetComponentInChildren <ParticleSystem> ();
		capsuleCollider = GetComponent <CapsuleCollider> ();
		currentHealth = startingHealth;
	}
	
	void Start(){
		killStateSet = new List<EnemyState.State>();
		stateSet = new Dictionary<EnemyState.State, EnemyState> ();
		
		/*
		EnemyBurn X = new EnemyBurn (this, 4, 1.0f, 20);
		addState (X);
		*/
	}
	
	void Update ()
	{
		if (isSinking) {
			transform.Translate (-Vector3.up * sinkSpeed * Time.deltaTime);
		} else {
			foreach(EnemyState X in stateSet.Values){
				X.updateState();
			}
			
			foreach(EnemyState.State X in stateSet.Keys){
				if (stateSet[X].effectCount <= 0){
					killStateSet.Add(X);
				}
			}
			
			while(killStateSet.Count > 0){
				stateSet.Remove(killStateSet[0]);
				killStateSet.Remove(0);
			}
			
		}
	}
	
	public void addState(EnemyState newState){
		if (stateSet.ContainsKey(newState.state) == false || stateSet[newState.state] == null) {
			stateSet.Add(newState.state, newState);
		} else {
			//It's fresh state
			stateSet[newState.state].ResetState(newState);
		}
	}
	
	public void TakeDamage (int amount, Vector3 hitPoint)
	{
		if(isDead){
			return;
		}
		enemyAudio.Play ();
		
		currentHealth -= amount;
		
		if (hitPoint != null) {
			hitParticles.transform.position = hitPoint; //move particle to hitpoint
			hitParticles.Play ();
		}
		
		if(currentHealth <= 0)
		{
			Death ();
		}
	}
	
	public void TakeDamageNoSound (int amount)
	{
		if (isDead){
			return;
		}
		currentHealth -= amount;
		
		if(currentHealth <= 0)
		{
			Death ();
		}
	}
	
	public void StateTakeDamage(int amount, EnemyState.State state){
		
		if(isDead){
			return;
		}
		currentHealth -= amount;
		
		ParticleSystem stateEff = this.getParticleByState (state);
		if (stateEff != null) {
			//stateEff.transform.position = this.transform.position; //move particle to hitpoint
			stateEff.Play ();
		}
		
		if(currentHealth <= 0)
		{
			Death ();
		}
		
	}
	
	private ParticleSystem getParticleByState(EnemyState.State state){
		switch (state) {
		case EnemyState.State.Burn: return burnEffect;
		case EnemyState.State.Slow: return slowEffect;
		case EnemyState.State.Stun: return stunEffect;
		default: return null;
		}
	}
	
	
	void Death ()
	{
		try{
		isDead = true;

		anim.SetBool("theDead",true);
		capsuleCollider.isTrigger = true; // when they die, they don't obstacle anymore
		
		anim.SetTrigger ("Dead");
		
		enemyAudio.clip = deathClip;
		enemyAudio.Play ();
		}catch(UnityException e){
			Debug.Log("bug of theDead is "+e);		
		}
	}
	
	// function in animation event (death)
	public void StartSinking ()
	{
		GetComponent <NavMeshAgent> ().enabled = false;
		GetComponent <Rigidbody> ().isKinematic = true; // ignore geometric calculatate
		isSinking = true;
		ScoreManager.score += scoreValue;
		Destroy (gameObject, 2f); // destroy after 2 seconds
	}
}
