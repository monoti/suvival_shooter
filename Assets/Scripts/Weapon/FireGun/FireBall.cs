﻿using UnityEngine;
using System.Collections;

public class FireBall : MonoBehaviour {

	public float TimeLife = 2.0f;

	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, TimeLife);
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate (Vector3.forward * Time.deltaTime);
	}

	void OnCollisionEnter(Collision obj){
		if (obj.gameObject.tag == "Enemy") {
			EnemyHealth enemy = obj.gameObject.GetComponent<EnemyHealth> ();
			enemy.TakeDamageNoSound (1);
			enemy.addState(new EnemyBurn(enemy, 5, 1.0f, 20));
		} else
			Destroy (this.gameObject);
	}
}
