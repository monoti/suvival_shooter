﻿using UnityEngine;
using System.Collections;

public class FireParticle : MonoBehaviour {

	public int damage = 10;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnParticleCollision(GameObject other) {
		if (other.tag == "Enemy") {
			EnemyHealth enemy = other.GetComponent <EnemyHealth>();
			enemy.TakeDamageNoSound((int) (damage*GamePlayerManager.damageFactor));
			enemy.addState(new EnemyBurn(enemy, 8, 0.5f, 5));
		}
	}
}
