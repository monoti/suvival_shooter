﻿using UnityEngine;
using System.Collections;

public class FireGun : MonoBehaviour {

	public GameObject fireBullet;
	public Transform gunPosition;
	public float timeBetweenBullets = 0.1f;

	private float timer = 0.0f;

	ParticleSystem gunParticles;
	Light gunLight;
	
	// Use this for initialization
	void Start () {
		gunParticles = GetComponent<ParticleSystem> ();
		gunLight = GetComponent<Light> ();
	}
	
	// Update is called once per frame
	void Update () {

		timer += Time.deltaTime;
		
		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets*GamePlayerManager.atkSpeedFactor && Time.timeScale != 0)
		{
			Shoot ();
			GameObject X = GameObject.FindGameObjectWithTag("GameManager");
			GamePlayerManager Y = X.GetComponent<GamePlayerManager>();
			Y.BulletAmount--;
		}else{
			DisableEffects ();
		}
	}

	void DisableEffects(){
		gunLight.enabled = false;
	}

	void Shoot(){
		timer = 0.0f;
		gunLight.enabled = true;
		
		gunParticles.Stop (); //if particle already started
		gunParticles.Play ();
		
		GameObject fire = (GameObject) GameObject.Instantiate( fireBullet ,this.transform.position, Quaternion.identity);
		fire.transform.rotation = this.transform.rotation;
	}
}
