﻿using UnityEngine;
using System.Collections;

public class IceController : MonoBehaviour {
	public GameObject bullet;
	public GameObject[] directions = new GameObject[8];
	private GameObject[] temps = new GameObject[8];
	public float timer = 0;

	public int damage = 10;
	public Vector3 hitPos;

	private bool killSelfOnce = true;
	
	void Start () {
		hitPos = transform.position;
		//Destroy (gameObject, 1.5f);
	}
	void Update () {
		timer = timer - Time.deltaTime;
		if (timer <= 0 && killSelfOnce) {
			createIce ();
			killSelfOnce = false;
			Destroy (this.gameObject);
		}
//		if (Input.GetMouseButtonDown(0)) {
//						for (int i =0; i<8; i++) {
//								temps [i] = (GameObject)GameObject.Instantiate (this.gameObject, directions [i].transform.position, Quaternion.identity);
//								temps [i].rigidbody.AddForce ((directions [i].transform.position - gameObject.transform.position).normalized * 50);
//						}	
//				}
	}

	void OnCollisionEnter(Collision obj){
		try{
		if (obj.gameObject.tag == "Enemy") {
			EnemyHealth enemy = obj.gameObject.GetComponent<EnemyHealth> ();
			enemy.TakeDamage ((int) (damage*GamePlayerManager.damageFactor), Vector3.up);
			createIce();
			//enemy.addState(new EnemyCold(enemy, 10, 0.5f));
			Destroy (this.gameObject);
		} 
		if(obj.gameObject.tag != "Player")
			Destroy (this.gameObject);
		}catch(UnityException e){
			Debug.Log("single ice:"+e);
		}
	}

	void createIce(){
		for (int i =0; i<8; i++) {
			if(directions[i] == null)
				return;
			temps [i] = (GameObject) GameObject.Instantiate (bullet, directions [i].transform.position, Quaternion.identity);
			temps [i].rigidbody.AddForce (( gameObject.transform.position- directions [i].transform.position ).normalized * 4000);
		}
	}
}
