﻿using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public int damagePerShot = 20;
    public float timeBetweenBullets = 0.15f;
    public float range = 100f;


    float timer; //keep everthing syncs
    Ray shootRay;
    RaycastHit shootHit;
    int shootableMask; // we can only hit shootable things
    ParticleSystem gunParticles;
    LineRenderer gunLine;
    AudioSource gunAudio;
    Light gunLight;
    float effectsDisplayTime = 0.2f;


    void Awake ()
    {
        shootableMask = LayerMask.GetMask ("Shootable");
        gunParticles = GetComponent<ParticleSystem> ();
        gunLine = GetComponent <LineRenderer> ();
        gunAudio = GetComponent<AudioSource> ();
        gunLight = GetComponent<Light> ();
    }


    void Update ()
    {
        timer += Time.deltaTime;

		if(Input.GetButton ("Fire1") && timer >= timeBetweenBullets*GamePlayerManager.atkSpeedFactor && Time.timeScale != 0)
        {
            Shoot ();
        }

        if(timer >= timeBetweenBullets * effectsDisplayTime) // time greater than effect time
        {
            DisableEffects (); // turn off the light
        }
    }


    public void DisableEffects ()
    {
        gunLine.enabled = false;
        gunLight.enabled = false;
    }


    void Shoot ()
    {
        timer = 0f; //reset the time

        gunAudio.Play ();

        gunLight.enabled = true;

        gunParticles.Stop (); //if particle already started
        gunParticles.Play ();

        gunLine.enabled = true;
        gunLine.SetPosition (0, transform.position); //start point position

        shootRay.origin = transform.position;
        shootRay.direction = transform.forward;

        if(Physics.Raycast (shootRay, out shootHit, range, shootableMask)) //if hit
        {
            EnemyHealth enemyHealth = shootHit.collider.GetComponent <EnemyHealth> ();
			if(enemyHealth != null) //if it hit enemy not others
            {
				enemyHealth.TakeDamage ((int) (damagePerShot*GamePlayerManager.damageFactor), shootHit.point);
            }
            gunLine.SetPosition (1, shootHit.point); //end point postion
        }
        else
        {
            gunLine.SetPosition (1, shootRay.origin + shootRay.direction * range);
        }
    }
}
