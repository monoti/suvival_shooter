﻿using UnityEngine;
using System.Collections;

public class PlayerState{

	public GamePlayerManager.EnumPlayerState buff;
	public float factor;
	public float duration;

	public PlayerState(GamePlayerManager.EnumPlayerState buff, float duration, float factor){
		this.buff = buff;
		this.duration = duration;
		this.factor = factor;
	}

	// Update is called once per frame
	public bool Update () {
		duration -= Time.deltaTime;
		return duration <= 0;
	}
}
