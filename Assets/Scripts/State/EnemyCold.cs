﻿using UnityEngine;
using System.Collections;

public class EnemyCold : EnemyState
{
	EnemyHealth enemyHealth;
	float normalSpeed;
	
	public EnemyCold(EnemyHealth enemy, int effectCount, float timeAffect)
	{
		base.state = State.Slow;
		enemyHealth = enemy;
		normalSpeed = enemyHealth.GetComponent<NavMeshAgent> ().speed;
		base.setState (effectCount, timeAffect);
	}
	
	public override void EffectState ()
	{
		this.enemyHealth.StateTakeDamage (0, base.state);
		enemyHealth.GetComponent<NavMeshAgent> ().speed = normalSpeed * 0.25f;
	}
	
	public override void RemoveState ()
	{
		enemyHealth.GetComponent<NavMeshAgent> ().speed = normalSpeed;

	}

	public override void ResetState (EnemyState newState)
	{
		base.effectCount = newState.effectCount;
	}
}
