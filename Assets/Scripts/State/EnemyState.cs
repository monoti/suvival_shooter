﻿using UnityEngine;

public abstract class EnemyState {

	public EnemyState.State state;
	public int effectCount = 0;
	protected float baseTimeAffect = 0;
	float effectTriggerTime = 0;

	public enum State {Burn, Stun, Slow};

	public abstract void EffectState ();
	public abstract void RemoveState ();
	public abstract void ResetState(EnemyState newState);

	public void updateState(){
		this.effectTriggerTime -= Time.deltaTime;
		if (this.effectTriggerTime <= 0) {
			this.effectCount -= 1;
			this.EffectState ();
			this.effectTriggerTime = this.baseTimeAffect;
		}
		if (this.effectCount <= 0)
			this.RemoveState ();
	}

	protected void setState(int stsCount, float baseTime){
		this.effectCount = stsCount;
		this.baseTimeAffect = baseTime;
		this.effectTriggerTime = this.baseTimeAffect;
	}

}
