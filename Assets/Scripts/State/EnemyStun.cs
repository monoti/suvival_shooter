﻿using UnityEngine;
using System.Collections;

public class EnemyStun : EnemyState
{

	EnemyHealth enemyHealth;
	EnemyAttack enemyAttack;
	float normalSpeed;
	
	public EnemyStun(EnemyHealth enemy, EnemyAttack enemyAtk, int effectCount, float timeAffect)
	{
		base.state = State.Stun;
		enemyHealth = enemy;
		enemyAttack = enemyAtk;
		normalSpeed = enemyHealth.GetComponent<NavMeshAgent> ().speed;
		base.setState (effectCount, timeAffect);
	}
	
	public override void EffectState ()
	{
		this.enemyHealth.StateTakeDamage (0, base.state);
		enemyAttack.noAttackAllow = true;
		enemyHealth.GetComponent<NavMeshAgent> ().speed = 0;
	}
	
	public override void RemoveState ()
	{
		enemyAttack.noAttackAllow = false;
		enemyHealth.GetComponent<NavMeshAgent> ().speed = normalSpeed;

	}

	public override void ResetState (EnemyState newState)
	{
		base.effectCount = newState.effectCount;
	}
}