using UnityEngine;

public class EnemyBurn : EnemyState
{
	EnemyHealth enemyHealth;
	int Damage;

	public EnemyBurn(EnemyHealth enemy, int effectCount, float timeAffect, int Damage)
	{
		this.Damage = Damage;
		base.state = State.Burn;
		enemyHealth = enemy;
		base.setState (effectCount, timeAffect);
	}

	public override void EffectState ()
	{
		this.enemyHealth.StateTakeDamage (Damage, base.state);
	}

	public override void RemoveState ()
	{

	}

	public override void ResetState (EnemyState newState)
	{
		base.effectCount = newState.effectCount;
	}
}


