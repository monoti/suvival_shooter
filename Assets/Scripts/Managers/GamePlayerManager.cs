﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GamePlayerManager : MonoBehaviour {

	public enum EnumPlayerState
		{
			ATK, AGI, SPD
		}

	public enum Weapon
		{
			Normal, Flame, Ice
		}

	public GameObject player;

	public Image hasteIcon;
	public Image freezeIcon;
	public Image doubleDamageIcon;
	public Image berserkIcon;

	public GameObject normalWeapon;
	public GameObject flameWeapon;
	public GameObject iceWeapon;

	public Text BulletText;
	public int BulletAmount = 10;

	public Weapon currectWeapon = Weapon.Normal;

	public static float damageFactor = 1.0f;
	public static float atkSpeedFactor = 1.0f;
	public static float movementSpeedFactor = 1.0f;

	private List<PlayerState> playerStateSet;


	void Awake(){
		playerStateSet = new List<PlayerState>();
	}

	// Use this for initialization
	void Start () {
		updateWeaponEffect();

	}
	
	// Update is called once per frame
	void Update () {
		if (BulletAmount <= 0) {
			BulletAmount = 999;
			currectWeapon = Weapon.Normal;
			updateWeaponEffect();
		}
		UpdateBulletText ();

		int i = 0;
		for(; i< playerStateSet.Count; i++){
			if(playerStateSet[i].Update()){
				BuffReverse(playerStateSet[i]);
				playerStateSet.RemoveAt(i);
			}else{
				i++;
			}
		}

	}

	private void updateWeaponEffect(){
		if (currectWeapon == Weapon.Normal) {
			normalWeapon.SetActive(true);
			flameWeapon.SetActive(false);
			iceWeapon.SetActive(false);
		}
		if (currectWeapon == Weapon.Flame) {
			normalWeapon.SetActive(false);
			flameWeapon.SetActive(true);
			iceWeapon.SetActive(false);
		}
		if (currectWeapon == Weapon.Ice) {
			normalWeapon.SetActive(false);
			flameWeapon.SetActive(false);
			iceWeapon.SetActive(true);
		}
	}

	public void getWeapon(Weapon weapon, int amount){
		if (currectWeapon != weapon) {
			currectWeapon = weapon;
			BulletAmount = amount;
				} else {
			BulletAmount += amount;
		}
		updateWeaponEffect ();
	}

	public void getBuff(EnumPlayerState Buff, float duration, float factor){
		Image X = getImagebyEnum (Buff);
		ItemCooldownManager Y = X.GetComponent<ItemCooldownManager> ();
		Y.setDuration (duration);
		changeByBuff (Buff, factor);
		playerStateSet.Add (new PlayerState(Buff, duration, factor));
	}

	private void BuffReverse(PlayerState Buff){
		foreach (PlayerState X in playerStateSet) {
			if(X != Buff && X.buff == Buff.buff)
				return;
		}
		changeByBuff (Buff.buff, 1.0f);
	}



	private void changeByBuff(EnumPlayerState buff, float factor){
		if (buff == EnumPlayerState.ATK)
			damageFactor = factor;
		if (buff == EnumPlayerState.SPD)
			movementSpeedFactor = factor;
		if (buff == EnumPlayerState.AGI)
			atkSpeedFactor = factor;
	}

	private void UpdateBulletText(){
		BulletText.text = "Bullet: " + BulletAmount;
	}

	private Image getImagebyEnum(EnumPlayerState buff){
		switch (buff) {
		case EnumPlayerState.SPD: return hasteIcon;
		case EnumPlayerState.ATK: return doubleDamageIcon;
		case EnumPlayerState.AGI: return berserkIcon;
		default: return null;
		}
	}
	
}
