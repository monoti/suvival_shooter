﻿using UnityEngine;
using System.Collections;

public class EnemySpawnPointManager : MonoBehaviour {

	public float baseSpawnTime;
	public float[] spawnChanceSet;
	public GameObject[] enemySet;
	public PlayerHealth playerHealth;

	private float timeConter = -1;

	// Update is called once per frame
	void Update () {
		this.timeConter = this.timeConter - Time.deltaTime;
		if (timeConter <= 0) {

		}
	}

	void Start ()
	{
		InvokeRepeating ("Spawn", baseSpawnTime, baseSpawnTime); //repeat function Spawn()
	}
	
	
	void Spawn ()
	{
		if(playerHealth.currentHealth <= 0f)
		{
			return;
		}

		int randSpawn = Random.Range(0, enemySet.Length);
		float randChange = Random.Range (0.0f, 1.0f);
		if (randChange <= spawnChanceSet[randSpawn]) {
			Instantiate (enemySet[randSpawn], this.transform.position, Quaternion.identity);
		}
	}
}
