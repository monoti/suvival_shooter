﻿using UnityEngine;
using System.Collections;

public class BossSpawningCheck : MonoBehaviour {

	public GameObject bossPrefab; 
	public float ScoreThreshold = 500;
	private bool isBossSpawn = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (!isBossSpawn && ScoreManager.score >= ScoreThreshold) {
			if(bossPrefab != null){
			 Instantiate(bossPrefab, this.transform.position, Quaternion.identity);
			}
			isBossSpawn = true;
		}
	}
}
