﻿using UnityEngine;
using System.Collections;

public class ShieldSpawnManager : MonoBehaviour {

	public float respawnTime;
	public GameObject itemPrefab;
	
	private GameObject itemObject = null;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", respawnTime, respawnTime); //repeat function Spawn()
	}

	void Spawn ()
	{
		if(itemObject == null){	
			GameObject newItem = (GameObject) Instantiate(itemPrefab, this.transform.position, Quaternion.identity);
			itemObject = newItem;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
