﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BulletManager : MonoBehaviour {

	public static int bulletNum; //it belongs to class

	Text text;
	
	void Awake ()
	{
		text = GetComponent <Text> ();
		bulletNum =  -1;
	}
	
	
	void Update ()
	{
		text.text = "Bullet: " + bulletNum;
	}
}
