﻿using UnityEngine;
using System.Collections;

public class ItemSpawnManager : MonoBehaviour {

	public float duration;
	public GamePlayerManager.EnumPlayerState buff;
	public float factor;
	public float respawnTime;
	public GameObject itemPrefab;

	private GameObject itemObject = null;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", respawnTime, respawnTime); //repeat function Spawn()
	}

	void Spawn ()
	{
		if(itemObject == null){	
			GameObject newItem = (GameObject) Instantiate(itemPrefab, this.transform.position, Quaternion.identity);
			BuffCollision X = newItem.GetComponent<BuffCollision>();
			X.buff = buff;
			X.duration = duration;
			X.factor = factor;
			itemObject = newItem;
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
