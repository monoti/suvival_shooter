﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class ItemCooldownManager : MonoBehaviour {

	public float duration = 0;
	private float t = 0;
	private bool isDecrease = true;

	Image image;
	// Use this for initialization
	void Awake () {
		image = gameObject.GetComponent<Image>();
		//image.color = new Color(0, 0, 0, 0f); // Some color
	}

	public void setDuration(float duration){
		this.duration = duration;
		this.t = duration;
		image.color = new Color(image.color.r, image.color.g, image.color.b, 255.0f);
		isDecrease = true;
	}

	void Start(){
		t = duration;
	}


	// Update is called once per frame
	void Update () {
		if (isDecrease) {
			float percent = t / duration;
			image.color = new Color (image.color.r, image.color.g, image.color.b, image.color.r * percent);
			t = t - Time.deltaTime;
			if(image.color.a <= 0){
				t = 0;
				isDecrease = false;
			}
		}
	}
}
