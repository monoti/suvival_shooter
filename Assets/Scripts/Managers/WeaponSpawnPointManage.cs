﻿using UnityEngine;
using System.Collections;

public class WeaponSpawnPointManage : MonoBehaviour {

	public GamePlayerManager.Weapon weapon;
	public int bullet;
	public float respawnTime;
	public GameObject itemPrefab;
	
	private GameObject itemObject = null;
	
	// Use this for initialization
	void Start () {
		InvokeRepeating ("Spawn", respawnTime, respawnTime); //repeat function Spawn()
	}
	
	void Spawn ()
	{
		if(itemObject == null){	
			GameObject newItem = (GameObject) Instantiate(itemPrefab, this.transform.position, Quaternion.identity);
			WeaponCollision X = newItem.GetComponent<WeaponCollision>();
			X.weapon = weapon;
			X.bullet = bullet;
			itemObject = newItem;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
