﻿using UnityEngine;
using System.Collections;

public class WeaponCollision : MonoBehaviour {

	public GamePlayerManager.Weapon weapon;
	public int bullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider obj){
		
		if (obj.gameObject.tag == "Player") 
		{
			GameObject X = GameObject.FindGameObjectWithTag("GameManager");
			GamePlayerManager Y = X.GetComponent<GamePlayerManager>();
			Y.getWeapon(weapon, bullet);
			Destroy(this.gameObject);
		}
		
	}
}
