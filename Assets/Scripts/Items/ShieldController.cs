﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShieldController : MonoBehaviour {


	public int startingShield = 50;
	public int currentShield;
	public Slider shieldSlider;
	PlayerHealth playerHealth;
	GameObject shieldGraphic;

	// Use this for initialization
	
	void Awake()
	{
		currentShield = 0;
		shieldSlider.value = currentShield;
		playerHealth = GetComponent<PlayerHealth> ();
	}
	void Start () {
	
	}
	void Update()
	{
		if (HaveShield()) 
		{
			shieldGraphic.transform.position = playerHealth.transform.position;
		}
	}
	public bool HaveShield ()
	{
		if (shieldGraphic != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public void TakeShieldDamage (int amount)
	{
		currentShield -= amount;
		shieldSlider.value = currentShield;

		if(currentShield <= 0)
		{
			playerHealth.currentHealth += currentShield;
			currentShield = 0;
			Destroy(shieldGraphic.gameObject);
		}
	}

	public bool hasShield(){
		return shieldGraphic != null;
	}

	public void getShield(GameObject shield){
		this.shieldGraphic = shield;
	}

	public void setShieldHealth(int amount){
		if (amount <= 0)
			this.currentShield = startingShield;
		shieldSlider.value = currentShield;
	}
}
