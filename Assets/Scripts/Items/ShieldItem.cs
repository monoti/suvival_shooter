﻿using UnityEngine;
using System.Collections;

public class ShieldItem : MonoBehaviour {

	public GameObject shieldPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision collision){

		if (collision.gameObject.tag == "Player") 
		{
			ShieldController X = collision.gameObject.GetComponent<ShieldController>();
			if(!X.hasShield()){
				GameObject newShield = (GameObject) Instantiate(shieldPrefab,this.transform.position,Quaternion.identity);
				X.getShield(newShield);
				X.setShieldHealth(-1);
			}
			else{
				X.setShieldHealth(-1);
			}
			Destroy(gameObject);
		}
		
	}
}
