﻿using UnityEngine;
using System.Collections;

public class BuffCollision : MonoBehaviour {

	public GamePlayerManager.EnumPlayerState buff;
	public float duration;
	public float factor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider obj){
		
		if (obj.gameObject.tag == "Player") 
		{
			GameObject X = GameObject.FindGameObjectWithTag("GameManager");
			GamePlayerManager Y = X.GetComponent<GamePlayerManager>();
			Y.getBuff(buff, duration, factor);
			Destroy(this.gameObject);
		}
		
	}
}
