﻿using UnityEngine;
using System.Collections;

public class TestParticleCollision : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnParticleCollision(GameObject other) {
		if (other.tag == "Enemy") {
			EnemyHealth X = other.GetComponent <EnemyHealth>();
			X.TakeDamage(2, Vector3.forward);
		}
	}
}
